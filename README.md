## Requirements and Prerequisites

1. A system with Terraform and kubectl installed.
2. An AWS account.
3. Knowledge of syntax and structure of the Terraform configuration file.
4. A version control system to manage Terraform code and track changes (e.g. Git).

This repository contains Terraform configuration files for deploying a set of modules in a specific order. The tfstate module must be deployed first, followed by main module.

## Deploying the AWS resources using Terraform Code

To deploy the this terraform code, navigate to the root directory and run the following command:

1. terraform init
2. terraform plan
3. terraform apply

Once you have provided the required input, Terraform will create the necessary resources for the tfstate module.

## VPC

The [squareops/vpc/aws](https://registry.terraform.io/modules/squareops/eks/aws/latest) module available on the Terraform Registry is designed to create and manage Amazon Virtual Private Cloud (VPC) resources in AWS (Amazon Web Services).

The module can be used to create a new VPC or use an existing one, along with its associated resources such as subnets, route tables, security groups, network ACLs, and Internet Gateway (IGW). It offers a simplified and standardized way to create VPC infrastructure, while also providing flexibility to customize VPC resources based on specific requirements.