locals {
  region      = "us-east-2"
  environment = "prod"
  name        = "skaf"
  additional_aws_tags = {
    Owner      = "SquareOps"
    Expires    = "Never"
    Department = "Engineering"
  }
  vpc_cidr = "172.10.0.0/16"
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source                                          = "squareops/vpc/aws"
  version                                         = "1.0.2"
  environment                                     = local.environment
  name                                            = local.name
  vpc_cidr                                        = local.vpc_cidr
  azs                                             = [for n in range(0, 2) : data.aws_availability_zones.available.names[n]]
  enable_public_subnet                            = true
  enable_private_subnet                           = false
  enable_database_subnet                          = false
  enable_intra_subnet                             = false
  one_nat_gateway_per_az                          = false
  vpn_server_enabled                              = false
  enable_flow_log                                 = false
}